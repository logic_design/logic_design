module Lab4_alu(
	input [2:0] a,
	input [2:0] b,
	input [1:0] op
	output reg [3:0] y
	);
	
	always @(*) begin
		case(op)
			2'b00 : y1 = a;
			2'b01 : y1 = a + b;
			2'b10 : y1 = a - b
			2'b11 : y1 = a << 1;
		endcase
	end
endmodule
