module Lab4_four_bit_multiplexor(
	input [3:0] d0,
	input [3:0] d1,
	input [3:0] d2,
	input [3:0] d3,
	input [1:0] s,
	output reg [3:0] y
	);
	
	always @(*) begin
		case (s)
			2'b00: y = d0;
			2'b01: y = d1;
			2'b10: y = d2;
			2'b11: y = d3;
		endcase
	end
endmodule
