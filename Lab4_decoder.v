module Lab4_decoder(
	input [1:0] s,
	input wren,
	output reg [0:3] y);
	
	always @(s, y) begin
		y = 4'b0000;
		if (wren) begin
			case(s)
				2'b00: y = 4'b1000;
				2'b01: y = 4'b0100;
				2'b10: y = 4'b0010;
				2'b11: y = 4'b0001;
			endcase
		end
	end
endmodule

	