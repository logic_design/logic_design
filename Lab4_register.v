module Lab4_register(
	input [3:0] din,
	input clk,
	input ld,
	output reg [3:0] dout
	);
	
	always @(posedge clk)
		if (ld)
			dout = din;
			
endmodule
